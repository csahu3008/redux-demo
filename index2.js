const redux=require('redux')
const BUY_CAKE='BUY_CAKE'
const createStore=redux.createStore
const combineReducers=redux.combineReducers
const BUY_ICECREAM='BUY_ICECREAM'
function buyCake(){
    return{
        type:BUY_CAKE,
        info:"first redux action"
    }
}  
// (prevState,action)=>newState
const buyIcream =()=>{
    return {
        type:BUY_ICECREAM,
        info:"Icecreams"
    }   
}
const initialCakeState={
    numOfCakes:10,
}
const initialIceCreamState={
    numOfIcecream:20
}
const cakeReducer =(state=initialCakeState,action)=>{
   
    switch(action.type){
        case BUY_CAKE:return {
            ...state,numOfCakes:state.numOfCakes-1
        }
        default:
            return state
    }
}

const IcecreamReducer=(state=initialIceCreamState,action)=>{
      switch(action.type){
            case BUY_ICECREAM:return {
            ...state,numOfIcecream:state.numOfIcecream-1
        }
        default:
                return state
    }
}


const rootReducer=combineReducers({
    cake:cakeReducer,
    icecream:IcecreamReducer
})

const store=createStore(rootReducer) 
console.log("Initial State",store.getState())

//subscribe
const unsubscribe=store.subscribe(()=>{
    console.log("update State",store.getState())

})


store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyIcream())
store.dispatch(buyIcream())
unsubscribe()

//Multiple Reducers combining into one
