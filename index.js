const redux=require('redux')
const reduxLogger=require('redux-logger')
const createStore=redux.createStore
const logger=reduxLogger.createLogger()
const applyMiddleWare=redux.applyMiddleware

const BUY_CAKE='BUY_CAKE'
const BUY_ICECREAM='BUY_ICECREAM'
function buyCake(){
    return{
        type:BUY_CAKE,
        info:"first redux action"
    }
}  
// (prevState,action)=>newState
const buyIcream =()=>{
    return {
        type:BUY_ICECREAM,
        info:"Icecreams"
    }   
}
const initialState={
    numOfCakes:10,
    numOfIcecream:100
}

const reducer =(state=initialState,action)=>{
   
    switch(action.type){
        case BUY_CAKE:return {
            ...state,numOfCakes:state.numOfCakes-1
        }
        case BUY_ICECREAM:return {
            ...state,numOfIcecream:state.numOfIcecream-1
        }
        default:
            return state
    }
}

const store=createStore(reducer,initialState,applyMiddleWare(logger)) 
console.log("Initial State",store.getState())

//subscribe
const unsubscribe=store.subscribe(()=>{
    console.log("update State",store.getState())

})


store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyIcream())
store.dispatch(buyIcream())
unsubscribe()
console.log(initialState)
