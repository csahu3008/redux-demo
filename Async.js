const redux=require('redux')

const createStore=redux.createStore
const applyMiddleWare=redux.applyMiddleware
const thunkMiddleWare=require('redux-thunk').default
const axios = require('axios')
const initialState={
     loading:false,
     users:[],
     error:""
    }
const FETCH_USERS_REQUEST='FETCH_USERS_REQUEST'
const FETCH_USERS_SUCCESS='FETCH_USERS_SUCCESS'
const FETCH_USERS_FAILURE='FETCH_USERS_FAILURE'

const fetchUsers=()=>{
    return {
        type:FETCH_USERS_REQUEST,
        info:"for checking whether request was made or not "
    }
}
const fetchSucess=(users)=>{
    return  {
        type:FETCH_USERS_SUCCESS,
        info:"request made was successfull",
        payload:users
    }
}
const fecthFailure=(err)=>{
    return {
        type:FETCH_USERS_FAILURE,
        info:'request made was unsuccessfull',
        payload:err
    
    }
}

const fetchCustomUsers=()=>{
    return function(dispatch){
        dispatch(fetchUsers()) 
        axios.get('https://jsonplaceholder.typicode.com/users').then(res=>{
              const users=res.data.map(user => user.name)
                dispatch(fetchSucess(users))
         }).catch(err=>{
            dispatch(fecthFailure(err.message))
         })
    }
}




const reducer =(state=initialState,action)=>{
    switch(action.type){
        case FETCH_USERS_REQUEST:
            return {
                ...state,loading:true
            }
        case FETCH_USERS_SUCCESS:
            return{
                ...state,
                loading:false,
                users:action.payload,
            }
        case FETCH_USERS_FAILURE:
            return{
             ...state,
             loading:false,
             error:action.payload
            }
    }
}


const myStore= createStore(reducer,applyMiddleWare(thunkMiddleWare))
console.log("Initial State",myStore.getState())

const unsubscribe=myStore.subscribe(()=>{
    console.log(myStore.getState())
})

myStore.dispatch(fetchCustomUsers())
